#include<iostream>
using namespace std;

void init(int grid[6][7]);	//display a board
int check(int grid[6][7],int win); // searching for winner

int main()
{
	int grid[6][7] = {}; // board
	int hold[7] = {5,5,5,5,5,5,5};	// store number of valid unit in each column
	int column; 
	int player = 1; // player1 plays first
	int win = 0; // game status
	
	cout<<"########################"<<endl;
	cout << "#Welcome to 'CONNECT 4'#"<<endl;
	cout<<"########################"<<endl;
	
	init(grid);
	
	do
	{		
		
		cout << "Player"<< player << " choose a column (1-7) : ";
		cin >> column;
		
		while(column<1 || column>7) // column must be 1-7
		{
			cout << "Invalid. Please choose again : ";
			cin >> column;
		}
		
		switch(column)
		{
			case 1 : 
				if(hold[0]<0) // column1 is full
				{
					cout << "Invalid." << endl;
					// swap player
					if(player==1){player =2;}
					if(player==2){player =1;}
				}
				else // there is empty unit in column1
				{
					grid[hold[0]][column - 1] = player;
					hold[0]--; // number of empty unit in column1 is decrease 
				}
				break;
				
			case 2 : 
				if(hold[1]<0) 
				{
					cout << "Invalid." << endl;
					if(player==1){player =2;}
					if(player==2){player =1;}
				}
				else 
				{
					grid[hold[1]][column - 1] = player;
					hold[1]--; 
				}
				break;
				
			case 3 : 
				if(hold[2]<0) 
				{
					cout << "Invalid." << endl;
					if(player==1){player =2;}
					if(player==2){player =1;}
				}
				else 
				{
					grid[hold[2]][column - 1] = player;
					hold[2]--; 
				}
				break;
				
			case 4 : 
				if(hold[3]<0) 
				{
					cout << "Invalid." << endl;
					if(player==1){player =2;}
					if(player==2){player =1;}
				}
				else 
				{
					grid[hold[3]][column - 1] = player;
					hold[3]--; 
				}
				break;
				
			case 5 : 
				if(hold[4]<0) 
				{
					cout << "Invalid." << endl;
					if(player==1){player =2;}
					if(player==2){player =1;}
				}
				else 
				{
					grid[hold[4]][column - 1] = player;
					hold[4]--; 
				}
				break;
				
			case 6 : 
				if(hold[5]<0) 
				{
					cout << "Invalid." << endl;
					if(player==1){player =2;}
					if(player==2){player =1;}
				}
				else 
				{
					grid[hold[5]][column - 1] = player;
					hold[5]--; 
				}
				break;
				
			case 7 : 
				if(hold[6]<0) 
				{
					cout << "Invalid." << endl;
					if(player==1){player =2;}
					if(player==2){player =1;}
				}
				else 
				{
					grid[hold[6]][column - 1] = player;
					hold[6]--; 
				}
				break;			
		}
		init(grid);//display game status
		
		//change player
		if(player==1){player =2;}
		else if(player==2){player =1;}
		
		//check to find winner
		win = check(grid, win);
		
	}while(win == 0);
	
	cout << "######################"<<endl;
	cout << "# The winner is "<<win<<"!!! #"<<endl;
	cout << "######################"<<endl;
	
	return 0;
}

void init(int grid[6][7])
{
	for(int a=1;a<8;a++)
	{
		cout<<" "<<a<<"|";
	}
	cout<<endl<<"---------------------"<<endl;
	for(int i=0 ; i<6 ; i++)
	{
		for(int j=0 ;j<7;j++)
		{
			if(grid[i][j]==0)	{cout << "  ";}
			else if(grid[i][j]==1)	{ cout<<" X";}					
			else if(grid[i][j]==2)	{ cout<<" O";}
			cout << "|";
		}
		cout <<endl <<"---------------------"<< endl ;
	}	
	cout << endl;	
}

int check(int grid[6][7],int win)
{
	int count = 1;
	for(int i = 0; i < 6; i++) 
	{
		for(int j = 0; j < 7; j++) 
		{
			if(grid[i][j]!=0)
			{
				for(int k = 0; k < 4; k++) 
				{
					for(int l = 0; l < 3; l++) 
					{
						if(k == 0 && j<4) // horizontal
						{
							if(grid[i][j] == grid[i][j + l + 1]){count++;}
							else
							{	
								count = 1;
								break;							
							}
						}
						else if(k == 1 && j<4 && i<3) // digonal
						{
							if (grid[i][j] == grid[i+l+1][j + l + 1]){count++;} 
							else
							{	
								count = 1;
								break;							
							}
						}
						else if(i<3 && k==2) // vertical
						{
							if(grid[i][j] == grid[i+l+1][j]){count++;}
							else							
							{
								count = 1;
								break;
							}
						}
						else if(i<3 && j>2 && k==3) // diagonal
						{
							if(grid[i][j] == grid[i +l+1][j-l-1]){count++;}
							else							
							{
								count = 1;
								break;
							}
						}
						if(count == 4) 
						{						
							win = grid[i][j];
							return win;
						}
					}
				}
			}
		}
	}
	return win;			
}
